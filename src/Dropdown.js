import React from 'react';
import Select from 'react-select';

const options = [
    { value: 'md', label: 'Maryland'}
];

class Dropdown extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            isToggleOn: true,
            selectedOption: null,
            listOpen: false,
            location: [
                {
                    id: 0,
                    title: 'MD',
                    selected: false,
                    key: 'location'
                },
            ]
        };

    }
    
    handleClick = (selectedOption) => {
        this.setState({ selectedOption });
        console.log(`Option selected:`, selectedOption);
        this.setState({
            isToggleOn: false
        })
    }

    render() {
        if (this.state.isToggleOn) {
            return <Select onChange={this.handleClick} onClick={this.handleClick} className="w-1/4 content-center" options = {options} />
        } else {
            return null
        }
    }   
}

export default Dropdown;