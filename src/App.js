import React from 'react';
import axios from 'axios';
import background from './background.jpg';
import Select from 'react-select';
import './App.css';

const options = [
  { value: 'md', label: 'Maryland'}
];

export default class App extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
        data: null,
        isToggleOn: true,
        selectedOption: null,
        listOpen: false,
        location: [
            {
                id: 0,
                title: 'MD',
                selected: false,
                key: 'location'
            },
          ]
      };
  } 

  componentDidMount() {
    axios.get('https://ky8suiws9c.execute-api.us-east-1.amazonaws.com/dev')
      .then(res => {
      console.log(res);
      const data = res.data;
      this.setState({data});
      });
    }

  handleClick = (selectedOption) => {
    this.setState({ selectedOption });
    console.log(`Option selected:`, selectedOption);
    this.setState({
      isToggleOn: false
    })
  }

  render() {
    if(!this.state.data)
    return "Loading...";
    
    if (this.state.isToggleOn) {
      return (
        <div className="flex flex-1 flex-col items-center justify-center" style={{backgroundImage: `url(${background})`, backgroundSize: "cover"}}>
          {this.state.isToggleOn && <Select onChange={this.handleClick} onClick={this.handleClick} className="w-1/4 content-center" options = {options} />}
        </div>
      )
    } else {
      return (
      <div className="flex flex-1 flex-col items-center justify-center">
        {!this.state.isToggleOn && <div className="text-center pt-100">
                <table className="border-t border-b border-gray-400 w-full my-40">
                    <thead class="bg-gray-700 text-white p-200 font-sans">
                    <tr>
                        <th class="font-normal py-2">River</th>
                        <th class="font-normal py-2">Temperature</th>
                        <th class="font-normal py-2">Streamflow</th>
                    </tr>
                    </thead>
                <tbody>
                    {this.state.data.map((river, index) =>
                    <tr><td class="border-t border-b border-gray-700 px-4 text-gray-800 font-sans font-200 py-2" key={index}>{river.river.S}</td><td class="border-t border-b border-gray-700 px-4 text-gray-800 font-sans font-200 py-2" key={index}>{river.temperature.S}</td><td class="border-t border-b border-gray-700 px-4 text-gray-800 font-sans font-200 py-2" key={index}>{river.streamflow.S}</td></tr>)}
                </tbody>
                </table> 
            </div>}
      </div>
      )
    }
  }
    //   )
    // }
    // return (
      // <div className="flex flex-1 flex-col items-center justify-center" style={{backgroundImage: `url(${background})`, backgroundSize: "cover"}}>
      //   {this.state.isToggleOn && <Select onChange={this.handleClick} onClick={this.handleClick} className="w-1/4 content-center" options = {options} />}
      //   {/* <Dropdown /> */}
      //   {!this.state.isToggleOn && <div className="text-center pt-100">
      //           <table className="border-t border-b border-gray-300 w-full my-40">
      //               <thead class="bg-gray-200 p-200">
      //               <tr>
      //                   <th class="font-normal py-1">River</th>
      //                   <th class="font-normal py-1">Location</th>
      //                   <th class="font-normal py-1">Temp</th>
      //                   <th class="font-normal py-1">CFS</th>
      //               </tr>
      //               </thead>
      //           <tbody>
      //               {this.state.data.map((river, index) =>
      //               <tr><td class="border-t border-b border-gray-300 px-4 font-200 py-1" key={index}>{river.river.S}</td><td class="border-t border-b border-gray300 px-4 font-light py-1" key={index}>{river.location.S}</td><td class="border-t border-b border-gray-300 px-4 font-light py-1" key={index}>{river.temp.S}</td><td class="border-t border-b border-gray-300 px-4 font-light py-1" key={index}>{river.cfs.S}</td></tr>)}
      //           </tbody>
      //           </table> 
      //       </div>}
      // </div>
    // );
//   }
}